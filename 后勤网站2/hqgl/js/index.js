// 分辨率大于1920网页自适应
(function(doc, win) {
    let docEl = doc.documentElement,
        resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize';
    let reCalc = function() {
        let clientWidth = docEl.clientWidth;
        if (!clientWidth) return;
        if(clientWidth>1900){
            docEl.style.fontSize = 21 * (clientWidth / 1900) + 'px';
        }
    };
    if (!doc.addEventListener) return;
    win.addEventListener(resizeEvt, reCalc, false);
    doc.addEventListener('DOMContentLoaded', reCalc, false);
})(document, window);

$(function (){
    //移动端下导航栏部分
    let guide=$("#guide>span:first-child");
    let guide2=$("#guide>span:last-child");
    let guides=$(".guide");
    let nav=$('.nav');
    let search=$('#search');
    // let guide_parent=$('.guide_parent');
    guide.on('click',function (){
        if($(guides[0]).css('display')==='none'){
            for(let i=0;i<7;i++) {
                $(guides[i]).css('display', 'block');
                $(guides[i]).css('background', '#f5f5f5');
                // $(guides[i]).addClass("col-xs-12");
            }
            nav.css('height','16rem');
            search.css({'display':'none'});
        }else {
            for(let i=0;i<7;i++) {
                $(guides[i]).css('display', 'none');
            }
            nav.css('height','2rem');
        }
    })

    guide2.on('click',function (){
        if(search.css('display')==='none'){
            search.css({'display':'block'});
            for(let i=0;i<7;i++) {
                $(guides[i]).css('display', 'none');
            }
            nav.css('height','4rem');
        }else {
            search.css({'display':'none'});
            nav.css('height','2rem');
        }
    })

    guides.each(function (i){
        $(this).click(function (){
            // if(parseInt($(this).css('height').replace(/[^0-9]/ig,""))>160){
            //     $(guides[i]).children('.sub').css('display','none');
            //     console.log('ceshi');
            // }
            // else {
            //     $(guides[i]).children('.sub').css('display','block');
            // }
            // if ($(guides[i]).children('.sub').css('display')==='block'){
            //     $(guides[i]).children('.sub').css('display','none');
            //     console.log('测试');
            // }
        })
    })

    // swiper组件配置
    let swiper = new Swiper(".mySwiper", {
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
            renderBullet: function (index, className) {
                // return '<span class="' + className + '">' + (index + 1) + "</span>";
                return '<span class="' + className + '">' + "</span>";
            },
        },
    });
})