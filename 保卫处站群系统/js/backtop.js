/*
 * @Description: 
 * @version: 
 * @Author: yyconion
 * @Date: 2021-12-10 17:59:41
 * @LastEditors: yyconion
 * @LastEditTime: 2021-12-10 18:03:36
 */
function WeixinTop() {
  this.init();
}
WeixinTop.prototype = {

  constructor: WeixinTop,
  init: function () {
    this._initBackTop();
  },
  _initBackTop: function () {
    let $backTop = this.$backTop = $('<div class="cbbfixed" style="z-index:9999999;">' +
      '<a class="gotop cbbtn">' +
      '<span class="up-icon"></span>' +
      '</a>' +
      '</div>');
    $('body').append($backTop);
    console.dir($backTop);
    $backTop.click(function () {
      console.log("123123");
      $("html, body").animate({
        scrollTop: 0
      }, 120);
    });
    var timmer = null;
    $(window).bind("scroll", function () {
      var d = $(document).scrollTop(),
        e = $(window).height();
      0 < d ? $backTop.css("bottom", "20px") : $backTop.css("bottom", "-90px");
      clearTimeout(timmer);
      timmer = setTimeout(function () {
        clearTimeout(timmer)
      }, 100);
    });
  }
}
var WeixinTop = new WeixinTop();